# Test Unity

Learning Unity in basic games
 1. Car simulator
 2. Fly Simulator
 3. Feed the animals
 4. Play Fetch

![Prototype1](/uploads/1ac0f6a2dd8d9d1ec8f4a5ad952636b3/Prototype1.gif)

**Car simulator**

![Prototype2](/uploads/27d8021c9ef4b833f31492d93be7773d/Prototype2.jpg)

**Fly Simulator**

![Снимок_экрана__7_](/uploads/7c0a2e7075aeb2f8e2e24dd8c4717612/Снимок_экрана__7_.png)

**Feed the animals**

![Снимок_экрана__9_](/uploads/7645d05eb5a6348920089163d1a3b27e/Снимок_экрана__9_.png)

**Play Fetch**