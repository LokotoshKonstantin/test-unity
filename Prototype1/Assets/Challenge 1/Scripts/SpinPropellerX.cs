﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinPropellerX : MonoBehaviour
{
  private float speedPropeller = 0.5f;
  // Start is called before the first frame update
  void Start()
  {
        
  }

  // Update is called once per frame
  void Update()
  {
    transform.RotateAroundLocal(Vector3.forward, speedPropeller);
  }
}
