﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
  public GameObject dogPrefab;
  private float time = 2.0f;
  private bool flag = false;

  void Start()
  {
    InvokeRepeating("AcceptRunDog", 2, time);
  }

  private void AcceptRunDog()
  {
    flag = true;
  }
  // Update is called once per frame
  void Update()
  {
    // On spacebar press, send dog
    if ((Input.GetKeyDown(KeyCode.Space)) && (flag == true))
    {
      Instantiate(dogPrefab, transform.position, dogPrefab.transform.rotation);
      flag = false;
    }
  }
}
