﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManage : MonoBehaviour
{
  public GameObject[] animalPrefabs;
  private float startDelay = 2;
  private float spawnInterval = 1.4f;
  // Start is called before the first frame update
  void Start()
  {
    InvokeRepeating("SpawnRandomAnimal", startDelay, spawnInterval);
  }

  // Update is called once per frame
  void Update()
  {

  }

  void SpawnRandomAnimal()
  {
    int current_index = Random.Range(0, 3);
    Instantiate(animalPrefabs[current_index], new Vector3(Random.Range(-30, 30), 0, 30), animalPrefabs[current_index].transform.rotation);
  }
}
